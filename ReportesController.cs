﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Prueba_Reportes_Martinrea.Models;
using Rotativa;
using Rotativa.Options;

namespace Prueba_Reportes_Martinrea.Controllers
{
    public class ReportesController : Controller
    {
        private ReportesContex db = new ReportesContex();

        // GET: Reportes
        public ActionResult Index()
        {
            var reportes = db.Reportes.Include(r => r.celda).Include(r => r.cliente).Include(r => r.pieza).Include(r => r.Turno);
            return View(reportes.ToList());
        }
        public FileContentResult GetImage(int id)
        {
            using (var context = new ReportesContex())
            {
                Pieza e = context.Piezas.SqlQuery("SELECT * FROM dbo.Piezas WHERE dbo.Piezas.id=" + id + ";").FirstOrDefault();
                if (e.mapaSoldadura != null)
                {
                    return File(e.mapaSoldadura, "image/jpeg");
                }
                return null;
            }
        }
        public FileContentResult GetImage2(int id)
        {
            using (var context = new ReportesContex())
            {
                Cliente e = context.Clientes.SqlQuery("SELECT * FROM dbo.Clientes WHERE dbo.Clientes.id=" + id + ";").FirstOrDefault();
                if (e.Logo != null)
                {
                    return File(e.Logo, "image/jpeg");
                }
                return null;
            }
        }
        private List<SelectListItem> _regionsItems;
        public ActionResult RegistroReportesSpot(Usuario usuario, ReporteSpot r)
        {
            using (ReportesContex _dbContext = new ReportesContex())
            {
                // CARGAMOS EL DropDownList DE REGIONES
                var regions = (from usr in db.Celdas
                               join rol in db.TipoSoldaduras on usr.Soldadura_Id equals rol.Id
                               where rol.Soldadura == "Spot"
                               select new
                               {
                                   Soldadura = rol.Soldadura,
                                   Celdas = usr.Celdas,
                                   Id = usr.Id
                               }).ToList();
                _regionsItems = new List<SelectListItem>();
                foreach (var item in regions)
                {
                    _regionsItems.Add(new SelectListItem
                    {
                        Text = item.Celdas,
                        Value = item.Id.ToString()
                    });
                }
                ViewBag.regionsItems = _regionsItems;
            }
            ViewBag.Usuario_Id = new SelectList(db.Usuarios, "Id", "nombre");

            ViewBag.Usuario_Id = new SelectList(db.Usuarios, "Id", "nombre");
            ViewBag.Cliente_Id = new SelectList(db.Clientes, "Id", "Clientes");
            ViewBag.Pieza = new SelectList(db.Piezas, "Id", "lado");
            ViewBag.TipoCategoriaTamaño_Id = new SelectList(db.TipoCategoriaTamaño, "Id", "Tamaño");
            ViewBag.CategoriaTamaño_Id = new SelectList(db.CategoriaTamaños, "Id", "Minimo");

            ViewBag.TipoCategoriaTamaño_Id2 = new SelectList(db.TipoCategoriaTamaño, "Id", "Tamaño");
            ViewBag.CategoriaTamaño_Id2 = new SelectList(db.CategoriaTamaños, "Id", "Minimo");

            ViewBag.Turno_Id = new SelectList(db.Turnos, "Id", "Turnos");

            return View();
        }
        [HttpGet]
        public JsonResult GetTerritoriesList(int Celda_Id)
        {
            using (ReportesContex _dbContext = new ReportesContex())
            {
                var territories = _dbContext.Piezas.Where(x => x.Celda_Id == Celda_Id).ToList();
                return Json(territories, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveOrder(ReporteSpot r,Prueba_Push_TuercaSpot pt, Prueba_Push_Tuerca_ClinchadoSpot pc)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ReportesContex dc = new ReportesContex())
                {
                    ReporteSpot reporte = new ReporteSpot {FechaPrueba=r.FechaPrueba,FechaJuliana=r.FechaJuliana
                    ,Turno_Id=r.Turno_Id,Usuarios=r.Usuarios,
                        Celda_Id=r.Celda_Id,Cliente_Id=r.Cliente_Id,Pieza_Id=r.Pieza_Id
                    ,HoraInicio=r.HoraInicio,HoraFin=r.HoraFin,Estado=r.Estado,
                        EstadoGeneral = r.EstadoGeneral
                    };

                 if(r.prueba_Push_TuercaSpots != null)
                    {
                        foreach (var i in r.prueba_Push_TuercaSpots)
                        {
                            dc.Prueba_Push_TuercaSpots.Add(i);
                        }
                    }
                
                     else  if (r.prueba_Push_Tuerca_ClinchadoSpots != null)
                    {
                        foreach (var i in r.prueba_Push_Tuerca_ClinchadoSpots)
                        {

                            dc.Prueba_Push_Tuerca_ClinchadoSpots.Add(i);

                        }
                    }

                        
                 
                       
                    
                  /*
                        foreach (var q in r.Prueba_Adeherencia_ProyeccionSpots)
                        {
                            dc.Prueba_Adeherencia_ProyeccionSpots.Add(q);

                        }
                    
                  
                        foreach (var v in r.Diametro_Soldadura_Spots)
                        {

                            dc.Diametro_Soldadura_Spots.Add(v);

                        }
                   
                
                        foreach (var z in r.Prueba_Fusion_StudSpot)
                        {
                            dc.Prueba_Fusion_StudSpots.Add(z);

                        }*/

         
                    r.EstadoGeneral = "En proceso";
                    r.Estado = "OK";
                    dc.Reportes.Add(reporte);
                    dc.SaveChanges();
                    status = true;         
                }
            }
            else
            {
                status = false;
            }
            ViewBag.Cliente_Id = new SelectList(db.Clientes, "Id", "Clientes", r.Cliente_Id);

            ViewBag.Turno_Id = new SelectList(db.Turnos, "Id", "Turnos", r.Turno_Id);
            ViewBag.TipoCategoriaTamaño_Id = new SelectList(db.TipoCategoriaTamaño, "Id", "Tamaño", pt.CategoriaTamaño_Id);
            ViewBag.CategoriaTamaño_Id = new SelectList(db.CategoriaTamaños, "Id", "Minimo", pt.TipoCategoriaTamaño_Id);

            ViewBag.TipoCategoriaTamaño_Id2 = new SelectList(db.TipoCategoriaTamaño, "Id", "Tamaño", pc.CategoriaTamaño_Id2);
            ViewBag.CategoriaTamaño_Id2 = new SelectList(db.CategoriaTamaños, "Id", "Minimo", pc.TipoCategoriaTamaño_Id2);

            return new JsonResult { Data = new { status = status } };
        }
      
        public ActionResult PrintPartialViewToPdf(int? id)
        {
            using (ReportesContex db = new ReportesContex())
            {
               
                ReporteSpot customer = db.Reportes.Include(r => r.celda).Include(r => r.cliente).Include(r => r.pieza).Include(r => r.Turno).FirstOrDefault(c => c.Id == id);
                ViewBag.ReporteSpot = db.Reportes.Where(d => d.Id == id).ToList();
               // ViewBag.Categories = db.Prueba_Push_TuercaSpots.Where(d => d.ReporteSpot_Id == id).Include(p => p.Categoria).ToList();
                ViewBag.Clinchado = db.Prueba_Push_Tuerca_ClinchadoSpots.Where(d => d.ReporteSpot_Id == id).ToList();
                ViewBag.Adeherencia = db.Prueba_Adeherencia_ProyeccionSpots.Where(d => d.ReporteSpot_Id == id).ToList();
                ViewBag.Diametro = db.Diametro_Soldadura_Spots.Where(d => d.ReporteSpot_Id == id).ToList();
                ViewBag.Fusion = db.Prueba_Fusion_StudSpots.Where(d => d.ReporteSpot_Id == id).ToList();
                ViewBag.Pieza = db.Reportes.Where(d => d.Pieza_Id == id).Include(r => r.pieza).ToList();

                var report = new PartialViewAsPdf("Details", customer)
                {
                    PageSize = Size.A2,
                    PageOrientation = Orientation.Landscape,
                    PageMargins = { Left = 50, Right = 50 }
                };
                return report;
            }

        }

    }
}
